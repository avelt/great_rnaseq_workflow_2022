#!/bin/bash

#sbatch -c 4 -p workq /work/avelt/great_rnaseq_workflow_2022/GREAT_run_automatic.sh

cd /home/avelt/data2/great_rnaseq_workflow_2022

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/etc/profile.d/conda.sh" ]; then
        . "/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/etc/profile.d/conda.sh"
    else
        export PATH="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<


conda activate great
export PERL5LIB=/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/envs/great/lib/5.26.2/


# I put resource bigfile = 1 on the rule download_fq_files
# and I constraint the snakemake job to 40 bigfile
# in order to finish 40 full jobs download->fastqc->salmon->remove fastq
# before downloading new files, in order to not fill all the disk space

# Using external rules in the main Snakefile (minimal Snakefile)
RULES=Snakefile_automatic_download
# Or with all rules included in one snakemake file:
#RULES=Snakefile.smk

# config file to be edited
CONFIG=GREAT_config.txt

# Generate the dag files
# With samples
#snakemake --resources bigfile=40 --configfile $CONFIG -s $RULES --dag | dot -Tpdf > dag.pdf

# Dry run (simulation)
#snakemake --resources bigfile=40 --configfile $CONFIG -s $RULES -np >snakemake_dryrun.out

TMPDIR=$(pwd)
# Full run (if everething is ok: uncomment it)
# --restart-times : Number of times to restart failing jobs (defaults to 0). -> useful for the download that failed
# --keep-going : Go on with independent jobs if a job fails.
snakemake --resources bigfile=10 --restart-times 5 --show-failed-logs --keep-going --cores 32 --configfile $CONFIG -s $RULES -p --rerun-incomplete

echo "GREAT run finished" | mail -s "GREAT run" amandine.velt@inrae.fr
