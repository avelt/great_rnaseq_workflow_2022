#!/bin/bash

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/etc/profile.d/conda.sh" ]; then
        . "/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/etc/profile.d/conda.sh"
    else
        export PATH="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

conda activate great
export PERL5LIB=/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/envs/great/lib/5.26.2/

cd /home/avelt/data2/great_rnaseq_workflow_2022/test_data/references_files/

# /cm/shared/apps/gffread-0.11.5.Linux_x86_64/gffread PN40024.v4.2.REF.gff3 -T -o PN40024.v4.2.REF.gtf
# hisat2_extract_splice_sites.py PN40024.v4.2.REF.gtf > PN40024.v4.2.REF.ss
# hisat2_extract_exons.py PN40024.v4.2.REF.gtf > PN40024.v4.2.REF.exon
# mkdir PN40024.v4.1_hisat2_index
cd PN40024.v4.2_hisat2_index
hisat2-build -p 16 --exon ../PN40024.v4.2.REF.exon --ss ../PN40024.v4.2.REF.ss ../PN40024.v4.REF.fasta PN40024.v4.2_hisat2_index
