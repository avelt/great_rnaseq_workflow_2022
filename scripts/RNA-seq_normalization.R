# Author : Amandine Velt (amandine.velt@inra.fr)
# Date : 25/04/2017

# This script takes as input a directory path containing all (and only) the RNA-seq raw count data tables (eg a directory with one htseq-count file per sample)
# and performs the DESeq normalization method (normalization by library size) and then calculates the RPKM.
#
# Usage :
# Complete command : /usr/bin/Rscript RNA-seq_normalization.R -f count_files_folder -b gene_name_attribute -o output_directory
# Minimal command : /usr/bin/Rscript RNA-seq_normalization.R -f count_files_folder -b gene_name_attribute -o output_directory
#
# Arguments description :
# count_files_folder -> directory containing all the raw count data tables (one per sample)
# gene_name_attribute -> the name of the attribute in the gtf referring to the gene information
# output -> directory where store the results
#################################################################################################################################################################

# check if libraries are installed, else they are installed
list.of.packages <- c("optparse", "tools", "locfit", "BiocManager", "feather", "DGEobj.utils")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) install.packages(new.packages, repos='http://cran.us.r-project.org')

list.of.packages <- c("edgeR")
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]
if(length(new.packages)) BiocManager::install(new.packages)

# libraries dependencies
suppressMessages(library("optparse"))
suppressMessages(library("feather"))
suppressMessages(library("tools"))
suppressMessages(library("edgeR"))
suppressMessages(library("DGEobj.utils"))

# options of the script
option_list = list(
  make_option(c("-f", "--folder"), type="character", default=NULL,
    help="[REQUIRED] Directory containing all the raw count data tables (one per sample)", metavar="character"),
  make_option(c("-o", "--output"), type="character", default=NULL,
    help="The directory where store the results. By default, the current directory. [default= %default]", metavar="character"),
  make_option(c("-t", "--cpm"), type="character", default=NULL,
    help="The final CPM normalized counts matrix filename and path. [default= %default]", metavar="character"),
  make_option(c("-r", "--raw"), type="character", default=NULL,
    help="The final raw counts matrix filename and path. [default= %default]", metavar="character"),
  make_option(c("-m", "--fpkm"), type="character", default=NULL,
    help="The final FPKM normalized counts matrix filename and path. [default= %default]", metavar="character"),
  make_option(c("-p", "--tpm"), type="character", default=NULL,
    help="The final TPM normalized counts matrix filename and path. [default= %default]", metavar="character"),
  make_option(c("-l", "--geneslength"), type="character", default=NULL,
    help="[REQUIRED] A file containing the genes length calculated by featureCounts", metavar="character")
);

# parsing of the arguments
opt_parser = OptionParser(option_list=option_list);
opt = parse_args(opt_parser);

# test the three essential arguments and exit if one of them in not given
if (is.null(opt$folder)){
  print_help(opt_parser)
  stop("At least one argument must be supplied (input folder).n", call.=FALSE)
}

# variable assignment
count_files_folder=opt$folder
output=opt$output
rawcounts_matrix=opt$raw
cpm_matrix=opt$cpm
fpkm_matrix=opt$fpkm
tpm_matrix=opt$tpm
genes_length=opt$geneslength

# for testing
# count_files_folder="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/FEATURE_COUNTS/FOR_R"
# output="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/FINAL_MATRIX"
# rawcounts_matrix="raw_counts"
# cpm_matrix="CPM_counts"
# fpkm_matrix="FPKM_counts"
# tpm_matrix="TPM_counts"
# genes_length="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/FEATURE_COUNTS/FOR_R/genes_length.txt"

# test of the output, if empty, give the current directory
if (is.null(output)){
  output=getwd()
}
# create output if doesn't exists
dir.create(output, showWarnings = FALSE)


###########################################################################################################################
# Creation of edgeR matrix counts
###########################################################################################################################

directory=count_files_folder

sampleFiles=grep("FEATURE_COUNTS_",list.files(directory),value=TRUE)
countData=readDGE(sampleFiles,header=FALSE, path=directory)
counts = as.data.frame(countData$counts)

rawcounts_to_write <- cbind(rownames(counts), data.frame(counts, row.names=NULL))
colnames_ok=gsub("HISAT2.FEATURE_COUNTS_", "", colnames(rawcounts_to_write))
colnames(rawcounts_to_write)=colnames_ok

write_feather(rawcounts_to_write, rawcounts_matrix)
write.table(rawcounts_to_write, file=paste(rawcounts_matrix,".tsv", sep=""), quote=FALSE, sep='\t', row.names=FALSE)

###########################################################################################################################
# Genes length calculated by featureCounts
###########################################################################################################################

exonic.gene.sizes=read.table(genes_length, row.names = 1, header=T)
colnames(exonic.gene.sizes)="gene_length_bp"

###########################################################################################################################
# Normalization part
###########################################################################################################################
merge_counts_length=merge(counts, exonic.gene.sizes, by="row.names")
rownames(merge_counts_length)=merge_counts_length[,1]
merge_counts_length=merge_counts_length[,-1]

counts=merge_counts_length[,-dim(merge_counts_length)[2]]
gene_length=merge_counts_length[dim(merge_counts_length)[2]]

dge = DGEList(counts=counts,genes=data.frame(Length=gene_length$gene_length_bp))
# The default method for computing these scale factors uses a trimmed mean of M-values (TMM) between each pair of samples
dge = calcNormFactors(dge)

# REMOVE RPKM AND REPLACE BY FPKM
# library size and gene length noramlization, to use for heatmap
# RPKM = rpkm(dge)
# RPKM_to_write <- cbind(rownames(RPKM), data.frame(RPKM, row.names=NULL))
# colnames(RPKM_to_write)=c("geneID", colnames(RPKM))
# write_feather(as.data.frame(RPKM_to_write), rpkm_matrix)
# write.table(as.data.frame(RPKM_to_write), file=paste(rpkm_matrix,".tsv", sep=""), quote=FALSE, sep='\t', row.names=FALSE)

# library size and gene length noramlization, to use for heatmap
CPM = convertCounts(as.matrix(counts),"CPM",as.vector(gene_length$gene_length_bp),log = FALSE,normalize = "TMM",prior.count = NULL)
CPM_to_write <- cbind(rownames(CPM), data.frame(CPM, row.names=NULL))
colnames_ok=gsub("HISAT2.FEATURE_COUNTS_", "", colnames(CPM))
colnames(CPM_to_write)=c("geneID", colnames_ok)
write_feather(as.data.frame(CPM_to_write), cpm_matrix)
write.table(as.data.frame(CPM_to_write), file=paste(cpm_matrix,".tsv", sep=""), quote=FALSE, sep='\t', row.names=FALSE)

# library size and gene length noramlization, to use for heatmap
FPKM = convertCounts(as.matrix(counts),"FPKM",as.vector(gene_length$gene_length_bp),log = FALSE,normalize = "TMM",prior.count = NULL)
FPKM_to_write <- cbind(rownames(FPKM), data.frame(FPKM, row.names=NULL))
colnames_ok=gsub("HISAT2.FEATURE_COUNTS_", "", colnames(FPKM))
colnames(FPKM_to_write)=c("geneID", colnames_ok)
write_feather(as.data.frame(FPKM_to_write), fpkm_matrix)
write.table(as.data.frame(FPKM_to_write), file=paste(fpkm_matrix,".tsv", sep=""), quote=FALSE, sep='\t', row.names=FALSE)

# source : https://rdrr.io/cran/DGEobj.utils/man/convertCounts.html
TPM <- convertCounts(as.matrix(counts),"TPM",as.vector(gene_length$gene_length_bp),log = FALSE,normalize = "TMM",prior.count = NULL)
TPM_to_write <- cbind(rownames(TPM), data.frame(TPM, row.names=NULL))
colnames_ok=gsub("HISAT2.FEATURE_COUNTS_", "", colnames(TPM))
colnames(TPM_to_write)=c("geneID", colnames_ok)
write_feather(as.data.frame(TPM_to_write), tpm_matrix)
write.table(as.data.frame(TPM_to_write), file=paste(tpm_matrix,".tsv", sep=""), quote=FALSE, sep='\t', row.names=FALSE)

