#!/bin/bash

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/etc/profile.d/conda.sh" ]; then
        . "/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/etc/profile.d/conda.sh"
    else
        export PATH="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

conda activate great
export PERL5LIB=/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/envs/great/lib/5.26.2/

cd /home/avelt/data2/great_rnaseq_workflow_2022/repair_fastq_files
mkdir -p /home/avelt/data2/great_rnaseq_workflow_2022/repair_fastq_files/raw

FASTQ_to_repair="SRR10973033
SRR10973034
SRR10973037
SRR17589115
SRR17589118
SRR2043187
SRR2043218
SRR2043221
SRR2043222
SRR2043223
SRR2043224"

# les échantillons sont forcément du paired-end

for accession in `echo $FASTQ_to_repair`
do
prefetch -O /home/avelt/data2/great_rnaseq_workflow_2022/repair_fastq_files/raw ${accession}

fastq-dump --outdir /home/avelt/data2/great_rnaseq_workflow_2022/repair_fastq_files/raw --split-files ${accession}

repair.sh in1=/home/avelt/data2/great_rnaseq_workflow_2022/repair_fastq_files/raw/${accession}_1.fastq \
in2=/home/avelt/data2/great_rnaseq_workflow_2022/repair_fastq_files/raw/${accession}_2.fastq \
out1=/home/avelt/data2/great_rnaseq_workflow_2022/repair_fastq_files/${accession}_1.fixed.fastq \
out2=/home/avelt/data2/great_rnaseq_workflow_2022/repair_fastq_files/${accession}_2.fixed.fastq \
outsingle=/home/avelt/data2/great_rnaseq_workflow_2022/repair_fastq_files/singletons.fq
done
