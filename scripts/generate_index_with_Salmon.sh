#!/bin/bash

conda activate great

cd /home/avelt/data2/great_rnaseq_workflow_2022
sed -i "s/ CDS=.*//" test_data/references_files/PN40024.v4.2.REF.transcripts.fasta

grep "^>" test_data/references_files/PN40024.v4.REF.fasta | cut -d " " -f 1 > test_data/references_files/decoys.PN40024.v4.2.txt
sed -i.bak -e 's/>//g' test_data/references_files/decoys.PN40024.v4.2.txt

cat test_data/references_files/PN40024.v4.2.REF.transcripts.fasta test_data/references_files/PN40024.v4.REF.fasta > test_data/references_files/PN40024.v4.2.REF.transcriptome_genome_for_Salmon.fa
gzip test_data/references_files/PN40024.v4.2.REF.transcriptome_genome_for_Salmon.fa

salmon index -t test_data/references_files/PN40024.v4.2.REF.transcriptome_genome_for_Salmon.fa.gz -d test_data/references_files/decoys.PN40024.v4.2.txt -p 10 -i test_data/references_files/PN40024.v4.2_salmon_index

