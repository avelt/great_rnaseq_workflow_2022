#!/bin/bash

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/bin/conda' 'shell.bash' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/etc/profile.d/conda.sh" ]; then
        . "/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/etc/profile.d/conda.sh"
    else
        export PATH="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

conda activate great
export PERL5LIB=/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_env/envs/great/lib/5.26.2/

cd /home/avelt/data2/great_rnaseq_workflow_2022/fastq_to_reanalyze

# ERR7421523 -> data non disponible sur le sra : https://trace.ncbi.nlm.nih.gov/Traces/sra/?run=ERR7421523

FASTQ_to_download="SRR12958469
SRR13726287
SRR13726470
SRR14697002
SRR15897949
SRR15897965
SRR15897985
SRR15897990
SRR15897992
SRR15897995
SRR15898001
SRR16311690
SRR5366121
SRR5366122
SRR5366123
SRR5366124
SRR5366125
SRR5366126
SRR5366127
SRR5366128
SRR5366129
SRR5366130
SRR5366131
SRR5366132
SRR5421606
SRR5457652
SRR5560666
SRR5560700
SRR5648416
SRR5851299
SRR5989152
SRR5989157
SRR6026707
SRR6189719
SRR6195043
SRR6365709
SRR6494891
SRR6706481
SRR6706486
SRR6951679
SRR6951694
SRR6951698
SRR7275237
SRR7424084
SRR7526697
SRR7526713
SRR7695760
SRR7695764
SRR7695766
SRR7768553
SRR7768562
SRR7768572
SRR7782911
SRR7849869
SRR8034265
SRR8263056
SRR8263071
SRR8507925
SRR8507931
SRR8507935
SRR8507941
SRR8508046
SRR8508076
SRR8536182
SRR8536186
SRR8560839
SRR8645429
SRR8645430
SRR8645431
SRR8645432
SRR866572
SRR8732043"

# les échantillons sont forcément du paired-end

for accession in `echo $FASTQ_to_download`
do
FIELDS="library_layout"
curl -s "https://www.ebi.ac.uk/ena/portal/api/filereport?result=read_run&fields=${FIELDS}&accession=${accession}" > ${accession}.ena.metadata.tsv
paired_or_not=$( tail -n1 ${accession}.ena.metadata.tsv | cut -f2 )
if [[ "$paired_or_not" == "SINGLE" ]]
then
prefetch -O /home/avelt/data2/great_rnaseq_workflow_2022/fastq_to_reanalyze ${accession}
fastq-dump --outdir /home/avelt/data2/great_rnaseq_workflow_2022/fastq_to_reanalyze ${accession}
else
prefetch -O /home/avelt/data2/great_rnaseq_workflow_2022/fastq_to_reanalyze ${accession}
fastq-dump --outdir /home/avelt/data2/great_rnaseq_workflow_2022/fastq_to_reanalyze --split-files ${accession}
fi
done

for file in `ls /home/avelt/data2/great_rnaseq_workflow_2022/fastq_to_reanalyze/*fastq`
do
gzip $file
done
