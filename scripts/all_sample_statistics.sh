#!/bin/bash

echo -ne "SRR_ID\tPE_SE\tTotal nb reads\t% overall mapping HISAT2\tnb alignements considered by featureCounts\tnb assigned alignments\t% assigned alignments\n" > all_samples_statistics.txt

for SRR in `cat /home/avelt/data2/great_rnaseq_workflow_2022/list_SRR.txt`
do
hisat2_file="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/HISAT2/$SRR/${SRR}.summary.txt"
if grep -q "were paired" $hisat2_file
then
PE_SE="PE"
total_nb_fragments=$( head -n1 $hisat2_file | cut -d" " -f1 )
total_nb_reads=$((2*total_nb_fragments))
else
PE_SE="SE"
total_nb_reads=$( head -n1 $hisat2_file | cut -d" " -f1 )
fi
percent_overall_mapping=$( grep "overall alignment rate" $hisat2_file | cut -d" " -f1 | sed "s/%//" )
featurecounts_file="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/LOGS/${SRR}.FEATURE_COUNTS.log"
nb_alignments_considered=$( grep "Total alignments" $featurecounts_file | cut -d":" -f2 | sed "s/||//" | tr -d " " )
nb_assigned_alignments=$( grep "assigned alignments" $featurecounts_file | cut -d":" -f2 | cut -d" " -f2 )
percent_nb_assigned_alignments=$( grep "assigned alignments" $featurecounts_file | cut -d":" -f2 | cut -d"(" -f2 | sed "s/%).*//" )
echo -ne "${SRR}\t${PE_SE}\t$total_nb_reads\t$percent_overall_mapping\t${nb_alignments_considered}\t${nb_assigned_alignments}\t${percent_nb_assigned_alignments}\t$percent_nb_reads_assigned\n" >> all_samples_statistics.txt
done

echo -ne "SRR_ID\tPE_SE\tTotal nb reads\t% overall mapping HISAT2\tnb alignements considered by featureCounts\tnb assigned alignments\t% assigned alignments\n" > all_samples_statistics_for_GREAT.txt

for SRR in `cat GREAT_analyses/FINAL_MATRIX/list_srr.txt`
do
  grep "$SRR" all_samples_statistics.txt >> all_samples_statistics_for_GREAT.txt
done
