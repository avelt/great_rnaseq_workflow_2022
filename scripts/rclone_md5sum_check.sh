#!/bin/bash


cd /home/avelt/data2/great_rnaseq_workflow_2022

for file in `cat scripts/2022_SRRs_list_VESPUCCI_GREAT.to_transfer.txt`
do
	if [ -f /home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/HISAT2/$file/${file}.sorted.bam.gz ]
	then
    filecopy="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/HISAT2/$file/${file}.sorted.bam.gz"
    md5sum $filecopy >> md5_bam_files_on_calcul.txt
	rclone md5sum GDrive_vespucci_own_API:${file}.sorted.bam.gz >> md5_bam_files_on_gdrive.txt
	fi
done

cd /home/avelt/data2/great_rnaseq_workflow_2022

for file in `cat scripts/2022_SRRs_list_VESPUCCI_GREAT.to_transfer.txt`
do
	if [ -f /home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/FEATURE_COUNTS/HISAT2.FEATURE_COUNTS_${file}.txt ]
	then
    filecopy="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/FEATURE_COUNTS/HISAT2.FEATURE_COUNTS_${file}.txt"
    md5sum $filecopy >> md5_featurecounts_files_on_calcul.txt
	rclone md5sum GDrive_vespucci_own_API:count_files/HISAT2.FEATURE_COUNTS_${file}.txt >> md5_featurecounts_files_on_gdrive.txt
	fi
done
# # cat ../2022_SRRs_list_VESPUCCI_GREAT.ILLUMINA.1.txt ../2022_SRRs_list_VESPUCCI_GREAT.ILLUMINA.2.txt ../2022_SRRs_list_VESPUCCI_GREAT.ILLUMINA.3.txt ../2022_SRRs_list_VESPUCCI_GREAT.ILLUMINA.4.txt > 2022_SRRs_list_VESPUCCI_GREAT.to_transfer.txt
# # sed -i "/SRR_ID/d" 2022_SRRs_list_VESPUCCI_GREAT.to_transfer.txt



# # diff md5_bam_files_on_calcul.txt md5_bam_files_on_gdrive.txt > diff_between_md5.txt

# while read line
# do
# md5_calcul=$( echo $line | tr " " "\t" | cut -f1 )
# filename_long_calcul=$( echo $line | tr " " "\t" | cut -f2 )
# filename_calcul=$( basename $filename_long_calcul )
# md5_gdrive=$( grep $filename_calcul md5_bam_files_on_gdrive.txt | cut -f1 -d" " )
# if [[ "$md5_calcul" == "$md5_gdrive" ]]
# then
# continue
# else
# echo "NOT SAME : $filename_calcul $md5_calcul $md5_gdrive"
# fi
# done < md5_bam_files_on_calcul.txt | wc -l
# # note : si rclone dit couldn't fetch token - maybe it has expired? - refresh with "rclone config reconnect GDrive_vespucci_own_API:": oauth2: cannot fetch token: 400 Bad Request
# # il faut reconnecter : rclone config reconnect GDrive_vespucci_own_API:

while read line
do
md5_calcul=$( echo $line | tr " " "\t" | cut -f1 )
filename_long_calcul=$( echo $line | tr " " "\t" | cut -f2 )
filename_calcul=$( basename $filename_long_calcul )
md5_gdrive=$( grep $filename_calcul md5_featurecounts_files_on_gdrive.txt | cut -f1 -d" " )
if [[ "$md5_calcul" == "$md5_gdrive" ]]
then
continue
else
echo "NOT SAME : $filename_calcul $md5_calcul $md5_gdrive"
fi
done < md5_featurecounts_files_on_calcul.txt | wc -l
# note : si rclone dit couldn't fetch token - maybe it has expired? - refresh with "rclone config reconnect GDrive_vespucci_own_API:": oauth2: cannot fetch token: 400 Bad Request
# il faut reconnecter : rclone config reconnect GDrive_vespucci_own_API:
