#!/bin/bash

SRR_list=$1

mkdir -p output_files

for SRR in `cat ${SRR_list}`
do
ACCESSION=$SRR
FIELDS="instrument_platform"
curl -s "https://www.ebi.ac.uk/ena/portal/api/filereport?result=read_run&fields=${FIELDS}&accession=${ACCESSION}" >> output_files/${ACCESSION}.ena.metadata.tsv
if grep -q "ILLUMINA" output_files/${ACCESSION}.ena.metadata.tsv
then
  echo $SRR >> output_files/liste_SRR_finale.txt
elif grep -q "BGISEQ" output_files/${ACCESSION}.ena.metadata.tsv
then
  echo $SRR >> output_files/liste_SRR_finale.txt
else
  platform=$( cat output_files/${ACCESSION}.ena.metadata.tsv )
  echo "$SRR is not ILLUMINA/BGISEQ but $platform" >> output_files/files_not_illumina_bgi.txt
fi
done
