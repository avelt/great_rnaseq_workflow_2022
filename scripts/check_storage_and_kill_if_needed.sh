#!/bin/bash

dir=$1
# en koctets
# if space < 1To, I kill the run
reqSpace=1000000000
# en koctets
availSpace=$(df "$dir" | awk 'NR==2 { print $4 }')
if (( availSpace < reqSpace )); then
  echo "NOSPACE"
else
  echo "SPACE"
fi
