#!/bin/bash
cd /data2/avelt/great_rnaseq_workflow_2022/GREAT_analyses/FINAL_MATRIX
# je supprime à la main tout les échantillons à No dans la colonne GREAT?
# je supprime à la main tous les échantillons non SRR (type CAR_BV, Vv01_AC07KNACXX_ACAGTG_L003_R1,...)
# je supprime SRR8645429, SRR8645430, SRR8645431, SRR8645432 car seulement 5 reads dans les fastq
cut -f4 RNA-Seq_datasets_COMPLET_12_07_2022.FOR_GREAT.txt | sed 1d > list_srr.txt
# certains échantillons sont dans cette liste mais pas dans la liste analysée car pas sélectionné par VESPUCCI et noté comme de mauvaise qualité chez nous, donc je les supprime
for sample in `cat list_srr.txt`; do if grep -q $sample ../../2022_SRRs_list_VESPUCCI_GREAT.ILLUMINA.txt; then continue; else sed -i "/$sample/d" list_srr.txt ; echo $sample; fi; done

# j'enlève les symboles bizarre à la main dans sublime, motif :  Â Â Â

R

# libraries dependencies
suppressMessages(library("optparse"))
suppressMessages(library("feather"))
suppressMessages(library("tools"))
suppressMessages(library("edgeR"))
suppressMessages(library("DGEobj.utils"))

# for testing
count_files_folder="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/FEATURE_COUNTS/FOR_R"
output="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/FINAL_MATRIX"
rawcounts_matrix="raw_counts_for_GREAT"
tpm_matrix="TPM_counts_for_GREAT"
genes_length="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/FEATURE_COUNTS/FOR_R/genes_length.txt"

directory=count_files_folder

sampleFiles=grep("FEATURE_COUNTS_",list.files(directory),value=TRUE)
countData=readDGE(sampleFiles,header=FALSE, path=directory)
counts = as.data.frame(countData$counts)

rawcounts_to_write <- cbind(rownames(counts), data.frame(counts, row.names=NULL))
colnames_ok=gsub("HISAT2.FEATURE_COUNTS_", "", colnames(rawcounts_to_write))
colnames(rawcounts_to_write)=colnames_ok

GREAT_database=read.table("/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/FINAL_MATRIX/RNA-Seq_datasets_COMPLET_12_07_2022.FOR_GREAT.txt", sep="\t", header=T, check.names=FALSE, encoding = 'UTF-8', quote="")
GREAT_database=GREAT_database[,c(1:26)]
GREAT_database[,sapply(GREAT_database,is.character)] <- sapply(GREAT_database[,sapply(GREAT_database,is.character)],iconv,"WINDOWS-1252","UTF-8")

srr_list=as.character(GREAT_database$File_name)
rawcounts_to_write_for_GREAT <- rawcounts_to_write[,-1]
rownames(rawcounts_to_write_for_GREAT) <- rawcounts_to_write[,1]
rawcounts_to_write_for_GREAT_OK=rawcounts_to_write_for_GREAT[, srr_list]
rawcounts_to_write_for_GREAT <- cbind(names = rownames(rawcounts_to_write_for_GREAT), rawcounts_to_write_for_GREAT)
names(rawcounts_to_write_for_GREAT)[names(rawcounts_to_write_for_GREAT) == 'names'] <- "geneID"

write_feather(rawcounts_to_write_for_GREAT, rawcounts_matrix)
write.table(rawcounts_to_write_for_GREAT, file=paste(rawcounts_matrix,".tsv", sep=""), quote=FALSE, sep='\t', row.names=FALSE)

###########################################################################################################################
# Genes length calculated by featureCounts
###########################################################################################################################

exonic.gene.sizes=read.table(genes_length, row.names = 1, header=T)
colnames(exonic.gene.sizes)="gene_length_bp"

###########################################################################################################################
# Normalization part
###########################################################################################################################
merge_counts_length=merge(rawcounts_to_write_for_GREAT[-1], exonic.gene.sizes, by="row.names")
rownames(merge_counts_length)=merge_counts_length[,1]
merge_counts_length=merge_counts_length[,-1]

counts=merge_counts_length[,-dim(merge_counts_length)[2]]
gene_length=merge_counts_length[dim(merge_counts_length)[2]]

# source : https://rdrr.io/cran/DGEobj.utils/man/convertCounts.html
TPM <- convertCounts(as.matrix(counts),"TPM",as.vector(gene_length$gene_length_bp),log = FALSE,normalize = "TMM",prior.count = NULL)
TPM_to_write <- cbind(rownames(TPM), data.frame(TPM, row.names=NULL))
colnames(TPM_to_write)=c("geneID", colnames(TPM))
write_feather(as.data.frame(TPM_to_write), tpm_matrix)
write.table(as.data.frame(TPM_to_write), file=paste(tpm_matrix,".tsv", sep=""), quote=FALSE, sep='\t', row.names=FALSE)

# verification des correspondances de srr_list, rawcounts_to_write_for_GREAT et TPM_to_write
srr_list=srr_list
srr_list_rawcounts=colnames(rawcounts_to_write_for_GREAT)
srr_list_TPM=colnames(TPM_to_write)
setdiff(srr_list, srr_list_rawcounts)
setdiff(srr_list, srr_list_TPM)
setdiff(srr_list_rawcounts, srr_list_TPM)
