ls GREAT_analyses/HISAT2/*/*bam.gz | sed "s/GREAT_analyses\/HISAT2\///" | sed "s/\/.*sorted.bam.gz//" > temp_list.txt

for sample in `cat 2022_SRRs_list_VESPUCCI_GREAT.ILLUMINA.1.txt | sed 1d`
do
  if grep -q "$sample" temp_list.txt
    then
      continue
    else
      echo "$sample"
  fi
done
