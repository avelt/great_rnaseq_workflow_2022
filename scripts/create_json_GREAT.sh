#!/bin/bash

# example : ./create_json_GREAT.sh ../test_data/liste_SRR.txt ../test_data/GREAT_database/RNA-Seq_datasets_USER_21.02.txt > ../test_data/GREAT_config.json

liste="$1"
database="$2"

if [ -z "$liste" ]
then
echo 'The first argument is empty. Command example : ./create_json_GREAT.sh ../test_data/liste_SRR.txt ../test_data/GREAT_database/RNA-Seq_datasets_USER_21.02.txt > ../test_data/GREAT_config.json' ; exit 1;
fi

if [ -z "$database" ]
then
echo 'The second argument is empty. Command example : ./create_json_GREAT.sh ../test_data/liste_SRR.txt ../test_data/GREAT_database/RNA-Seq_datasets_USER_21.02.txt > ../test_data/GREAT_config.json' ; exit 1;
fi

echo '{
"workdir": "/data2/GREAT/GREAT_analyses/",
"scriptsdir": "/data2/GREAT/great_rnaseq_workflow_2022/scripts/",
"rbin": "/cm/shared/apps/R-3.5.0/bin",
"references": {
"genome": "/data2/GREAT/PN12Xv2_reference_genome/index_PN12XV2_VCostv3/",
"annotations_CRIBI": "/data2/GREAT/PN12Xv2_reference_genome/VCost.v3_20.formatted.gff3"
},
"samples": {'

nb=$( wc -l $liste | cut -d" " -f1 )
nb_SRR=$(($nb+1))

COUNTER=0
for SRR in `cat $liste`
do
COUNTER=$[$COUNTER +1]
bioproject=$( grep $SRR $database | cut -f1 )
first_author=$( grep $SRR $database | cut -f2 )
paper=$( grep $SRR $database | cut -f3 )
private_public=$( grep $SRR $database | cut -f5 )
sequencing_platform=$( grep $SRR $database | cut -f6 )
PE_SE=$( grep $SRR $database | cut -f7 )
read_length=$( grep $SRR $database | cut -f8 )
protocol_library=$( grep $SRR $database | cut -f9 )
stranded=$( grep $SRR $database | cut -f10 )
species=$( grep $SRR $database | cut -f11 )
variety=$( grep $SRR $database | cut -f12 )
organ=$( grep $SRR $database | cut -f13 )
tissue=$( grep $SRR $database | cut -f14 )
short_stage=$( grep $SRR $database | cut -f15 )
detailed_stage=$( grep $SRR $database | cut -f16 )
added_date=$( grep $SRR $database | cut -f17 )
experimentation_type=$( grep $SRR $database | cut -f18 )
treatment=$( grep $SRR $database | cut -f19 )
treatment_type=$( grep $SRR $database | cut -f20 )
treatment_nature=$( grep $SRR $database | cut -f21 )
treatment_detail=$( grep $SRR $database | cut -f22 )
comments=$( grep $SRR $database | cut -f23 )
edit_distance=$(($read_length*5/100))
if [[ "$stranded" == "No" || "$stranded" == "no" ]]
then
protocol_library_featureCounts=0
elif [[ "$stranded" == "reverse" ]]
then
protocol_library_featureCounts=2
fi

path_sra="/data2/GREAT/GREAT_rawdata/$SRR.sra"

if [[ "$COUNTER" -ne "$nb_SRR" ]]
then
echo "\"$SRR\": {\"input_file\" : [\"$path_sra\"],\"bioproject\" : [\"$bioproject\"],\"first_author\" : [\"$first_author\"],\"paper\" : [\"$paper\"],\"private_public\" : [\"$private_public\"],\"sequencing_platform\" : [\"$sequencing_platform\"],\"PE_SE\" : [\"$PE_SE\"],\"read_length\" : [\"$read_length\"],\"protocol_library\" : [\"$protocol_library\"],\"stranded\" : [\"$stranded\"],\"protocol_library_featureCounts\" : [\"$protocol_library_featureCounts\"],\"species\" : [\"$species\"],\"variety\" : [\"$variety\"],\"organ\" : [\"$organ\"],\"tissue\" : [\"$tissue\"],\"short_stage\" : [\"$short_stage\"],\"detailed_stage\" : [\"$detailed_stage\"],\"added_date\" : [\"$added_date\"],\"experimentation_type\" : [\"$experimentation_type\"],\"treatment\" : [\"$treatment\"],\"treatment_type\" : [\"$treatment_type\"],\"treatment_nature\" : [\"$treatment_nature\"],\"treatment_detail\" : [\"$treatment_detail\"],\"comments\" : [\"$comments\"],\"edit_distance\" : [\"$edit_distance\"]},"
else
echo "\"$SRR\": {\"input_file\" : [\"$path_sra\"],\"bioproject\" : [\"$bioproject\"],\"first_author\" : [\"$first_author\"],\"paper\" : [\"$paper\"],\"private_public\" : [\"$private_public\"],\"sequencing_platform\" : [\"$sequencing_platform\"],\"PE_SE\" : [\"$PE_SE\"],\"read_length\" : [\"$read_length\"],\"protocol_library\" : [\"$protocol_library\"],\"stranded\" : [\"$stranded\"],\"protocol_library_featureCounts\" : [\"$protocol_library_featureCounts\"],\"species\" : [\"$species\"],\"variety\" : [\"$variety\"],\"organ\" : [\"$organ\"],\"tissue\" : [\"$tissue\"],\"short_stage\" : [\"$short_stage\"],\"detailed_stage\" : [\"$detailed_stage\"],\"added_date\" : [\"$added_date\"],\"experimentation_type\" : [\"$experimentation_type\"],\"treatment\" : [\"$treatment\"],\"treatment_type\" : [\"$treatment_type\"],\"treatment_nature\" : [\"$treatment_nature\"],\"treatment_detail\" : [\"$treatment_detail\"],\"comments\" : [\"$comments\"],\"edit_distance\" : [\"$edit_distance\"]}"
fi
done
echo "}"
