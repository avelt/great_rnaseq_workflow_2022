#!/bin/bash

# cat ../2022_SRRs_list_VESPUCCI_GREAT.ILLUMINA.1.txt ../2022_SRRs_list_VESPUCCI_GREAT.ILLUMINA.2.txt ../2022_SRRs_list_VESPUCCI_GREAT.ILLUMINA.3.txt ../2022_SRRs_list_VESPUCCI_GREAT.ILLUMINA.4.txt > 2022_SRRs_list_VESPUCCI_GREAT.to_transfer.txt
# for final copy :  ls -d GREAT_analyses/HISAT2/* | cut -f"3" -d"/" > 2022_SRRs_list_VESPUCCI_GREAT.to_transfer.txt
# sed -i "/SRR_ID/d" 2022_SRRs_list_VESPUCCI_GREAT.to_transfer.txt 

for file in `cat /home/avelt/data2/great_rnaseq_workflow_2022/to_copy_gdrive.txt`
do
filecopy="/home/avelt/data2/great_rnaseq_workflow_2022/GREAT_analyses/HISAT2/$file/${file}.sorted.bam.gz"
rclone copy $filecopy GDrive_vespucci_own_API:
done

# note : si rclone dit couldn't fetch token - maybe it has expired? - refresh with "rclone config reconnect GDrive_vespucci_own_API:": oauth2: cannot fetch token: 400 Bad Request
# il faut reconnecter : rclone config reconnect GDrive_vespucci_own_API:

